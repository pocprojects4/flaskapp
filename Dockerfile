# Install operating system
FROM ubuntu

# Install python pip
RUN apt-get update && apt-get install -y python3-dev python3-pip

# Install application dependencies
WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

EXPOSE 5000
# Start Application

CMD ["python3","app.py"]
